DataEvent {
	var <>time, <>val, <>id;

	*new { |time = 0.0, val = 0, id = 0|
		^super.newCopyArgs(time, val, id)
	}

	isEqualTo { |dataEvent| ^this.array == dataEvent.array }

	array { ^[time, val, id] }
	starttime_ { |newTime| this.time_(newTime) }
	starttime { ^this.time }
}


DataTrack {
	var <>id, <>events;
	var <>nextIndex;

	*new { |id, events|
		^super.newCopyArgs(id, events).init
	}

	init {
		id = id ?? { UniqueID.next };
		events = events ?? { [] };
		this.resetStream
	}

	add { |event, sort = false|
		event.isKindOf(Array).not.if({ event = [event] });
		event.do{ |thisEvent| events = events.add(thisEvent) };
		sort.if({ this.sort })
	}

	at { |index| ^events[index] }

	times { ^events.collect(_.time) }
	starttimes { ^this.times }
	times_ { |times| times = times.asArray; events.do{ |event, i| event.time_(times.wrapAt(i)) } }
	starttimes_ { |starttimes| this.times_(starttimes) }
	vals { ^events.collect(_.val) }
	vals_ { |vals| vals = vals.asArray; events.do{ |event, i| event.val_(vals.wrapAt(i)) } }
	ids { ^events.collect(_.id) }
	ids_ { |ids| ids.isKindOf(Array).not.if({ ids = [ids] }); events.do{ |event, i| event.id_(ids.wrapAt(i)) } }
	convertIDs { this.ids_(this.id) }

	size { ^this.events.size }

	next { |index = 0|
		nextIndex = nextIndex + 1;
		^this.events.wrapAt(index + nextIndex - 1);
	}

	isEqualTo { |dataTrack|
		var otherTrack = dataTrack.events;
		^this.events.every({ |item, i| item.isEqualTo(otherTrack[i]) })
	}


	resetStream { nextIndex = 0 }
	sort { events = events.sort{ |a, b| a.time < b.time } }

	duration {
		var lastEvent; this.sort; lastEvent = events.last;
		^lastEvent.respondsTo('dur').if({ lastEvent.time + lastEvent.dur }, { lastEvent.time })
	}

	plot { |dataType = 'val', eventType = 'events', db = false|
		this.plotEnv(dataType, eventType, db).plot
	}

	plotEnv { |dataType = 'val', eventType = 'events', db = false|
		var theseEvents, levels, times;
		theseEvents = this.perform(eventType);
		^theseEvents.isEmpty.not.if({
			levels = [theseEvents[0].perform(dataType)];
			times = [];
			theseEvents.do{ |event, i|
				levels = levels.add(event.perform(dataType));


				(i == 0).if({
					times = times.add(event.starttime)
				}, {
					times = times.add(event.starttime - theseEvents[i - 1].starttime)
				})



			};
			db.if({
				levels = (levels/127).ampdb
			});
			Env(levels, times)
		});
	}

}

DataEvents {
	var <>tracks, <>trackType;

	*new { |tracks, trackType|
		^super.newCopyArgs(tracks, trackType).init
	}

	*read { |filePath|
		var tempEvents;
		tempEvents = Object.readArchive(filePath);
		^super.newCopyArgs(tempEvents.tracks, tempEvents.trackType).init
	}

	init{
		trackType = trackType ?? { DataTrack };
		tracks.isNil.if({ tracks = [] });
		tracks.isKindOf(SequenceableCollection).not.if({ tracks = [tracks] })
	}

	sort { tracks.do{ |track| track.sort } }
	at { |index| ^this.tracks[index] }
	size { ^this.tracks.size }
	sizes {  ^this.tracks.collect(_.size) }
	trackIDs { ^this.tracks.collect{ |track| track.id } }
	trackIDs_ { |newTrackIDs|
		this.tracks.do{ |track, i|
			track.name_(newTrackIDs[i])
		}
	}

	addTrack { |track|
		track.isNil.if({ tracks = tracks.add(trackType.new) }, { tracks = tracks.add(track) })
	}

	events { |track|
		track = track ?? { (0..tracks.size-1) };
		^tracks[track].asArray.collect{ |thisTrack| thisTrack.events }
	}

	duration {
		var lastEvent = this.events.flatten.sort{ |a, b| a.time < b.time }.last;
		^lastEvent.respondsTo('dur').if({ lastEvent.time + lastEvent.dur }, { lastEvent.time })
	}

	removeEmptyTracks { |eventType = 'events'|
		var indices = [];
		this.tracks.do{ |track, i|
			track.perform(eventType).isEmpty.not.if({
				indices = indices.add(i)
			})
		};
		this.tracks_(this.tracks[indices]);
	}

	splitTracks {
		var splitTracks = [], trackNames;
		trackNames = this.events.flatten.collect{ |event| event.id }.asSet.asArray;

		trackNames.do{ |trackName, i| splitTracks = splitTracks.add(trackType.new(trackName)) };
		this.events.flatten.do{ |event, eventNumber|
			var index = trackNames.indexOfEqual(event.id);
			splitTracks[index].add(event)
		};
		this.tracks_(splitTracks).sort
	}

	add { |newEvents|
		newEvents.tracks.do{ |track, trackNumber|
			case
			// both events have tracks
			{ this.tracks[trackNumber].notNil and: newEvents.tracks[trackNumber].notNil } {
				this.tracks[trackNumber].add(track.events).sort
			}
			// only new events has the track
			{ this.track[trackNumber].isNil and: newEvents.tracks[trackNumber].notNil } {
				this.addTrack(track)
			}
		}
	}

	isEqualTo { |dataEvents|
		^this.tracks.collect{ |track, k|
			var otherEvents = dataEvents[k].events;
			track.events.every({ |item, i| item.isEqualTo(otherEvents[i]) })

		}.every({ |item| item })
	}

	checkTrackNames {
		var newTrackIDs;
		newTrackIDs = this.trackIDs;
		this.trackIDs.do{ |thisTrackID, i|
			var indices, inc = 1;
			indices = newTrackIDs.indicesOfEqual(thisTrackID);
			(indices.size > 1).if({
				indices.do{ |index|
					newTrackIDs[index] = thisTrackID ++ '_' ++ inc;
					inc = inc + 1
				}
			})
		};
		this.trackIDs_(newTrackIDs)
	}

	plot { |dataType = 'val', eventType = 'events', trackNumbers, db = false, bounds = (Window.screenBounds)|
		var plotter, plotValue = [], totalDuration, dataEnv;
		trackNumbers = trackNumbers ?? { (0..tracks.size-1) };
		plotter = Plotter(dataType.asString, bounds);
		dataEnv = this.plotEnv(dataType, eventType, trackNumbers, db);
		totalDuration = dataEnv.collect{ |env| env.totalDuration }.maxItem;
		plotter.value = dataEnv.collect{ |env|
			env.pad(totalDuration).asMultichannelSignal.flop
		};
		plotter.domainSpecs = ControlSpec(0, totalDuration, units: "s");
		plotter.setProperties(\labelX, "time");
		plotter.refresh

	}

	plotEnv  { |dataType = 'val', eventType = 'events', trackNumbers, db = false|
		var envArray;
		tracks[trackNumbers].do{ |track|
			var env = track.plotEnv(dataType, eventType, db);
			env.notNil.if({ envArray = envArray.add(env) });
		};
		^envArray
	}

	save { |filePath|
		this.writeArchive(filePath)
	}

	pythonPlot { |dataType = 'val', eventType = 'events', trackNumbers, verbose = false|
		var pythonPlot, allevents, theseTrackIDs, titles, data;
		this.checkForPython;
		trackNumbers = trackNumbers ?? { (0..tracks.size-1) };
		allevents = tracks[trackNumbers].asArray.collect{ |track|
			track.perform(eventType).collect{ |event| [event.time, event.perform(dataType)] }.flop
		};
		trackNumbers = [];
		allevents.do{ |events, i|
			events[0].notEmpty.if({ trackNumbers = trackNumbers.add(i) })
		};
		allevents = allevents[trackNumbers];
		theseTrackIDs = this.trackIDs[trackNumbers];
		pythonPlot = PythonPlot.new;
		pythonPlot.plot(allevents, dataType, 'seconds', theseTrackIDs, verbose)

	}

	pythonHist { |dataType = 'val', eventType = 'events', trackNumbers, verbose = false|
		var pythonPlot, allevents, theseTrackIDs;
		this.checkForPython;
		trackNumbers = trackNumbers ?? { (0..tracks.size-1) };
		allevents = tracks[trackNumbers].asArray.collect{ |track|
			track.perform(eventType).collect{ |event| event.perform(dataType) }
		};
		trackNumbers = [];
		allevents.do{ |events, i|
			events[0].notEmpty.if({ trackNumbers = trackNumbers.add(i) })
		};
		allevents = allevents[trackNumbers];
		theseTrackIDs = this.trackIDs[trackNumbers];
		pythonPlot = PythonPlot.new;
		pythonPlot.histogram(allevents, dataType, theseTrackIDs, verbose)

	}

	checkForPython {
		Quarks.isInstalled("PythonSC").not.if(
			{Error.new("PythonSC Quark not installed").throw}
		)
	}


}



