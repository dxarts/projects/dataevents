DataProcessor {
	var <>dataEvents;
	var <prevNow, <prevIndex;
	var <ftk, <>y1 = 0.0, <>y2 = 0.0;

	*new { |dataEvents|
		^super.newCopyArgs(dataEvents).init
	}

	init {
		ftk = FTK.new
	}

	indexEnv { |indexEnv, minDur = 0.01|
		var minLevel, maxLevel, theseEvents;
		var dataEventsDuration, originalEvents;
		originalEvents = dataEvents.deepCopy;
		originalEvents.tracks.do{ |track| track.events_([]) };
		dataEventsDuration = dataEvents.duration;
		// get the minimum time in seconds of the desired events
		minLevel = indexEnv.levels.minItem * dataEventsDuration;
		// get the maximum time in seconds of the desired events
		maxLevel = indexEnv.levels.maxItem * dataEventsDuration;
		dataEvents.tracks.do{ |track, trackNumber|
			// select only events within the deisered range for efficiency reasons
			theseEvents = track.events.select({ |event| event.time.inclusivelyBetween(minLevel, maxLevel) });
			// for each event, find the times at which it should be played according to the indexEnv
			theseEvents.do{ |event|
				var time, newTimes, dataEvent, dur, slope;
				// get the time scaled to 0 to 1 of the current event
				time = event.time/dataEventsDuration;
				// find all times where the indexEnv crosses the time of the event
				newTimes = indexEnv.atLevel(time);
				// for all found times, add the event to the newTracks
				newTimes.do{ |dataEventsTime|
					// calculate the slope to scale the duration
					slope = indexEnv.derivative(dataEventsTime).abs * dataEventsDuration;

					dataEvent = event.copy;
					// scale the dur if appropriate
					dataEvent.respondsTo('dur').if({
						dataEvent.dur_(minDur.max(event.dur/slope))
					});
					originalEvents.tracks[trackNumber].add(dataEvent.time_(dataEventsTime))
				}
			};

			originalEvents.tracks[trackNumber].sort
		};
		this.dataEvents_(originalEvents.removeEmptyTracks)
	}

	index { |index, minDur = 0.01|
		var now, newEvents;
		prevNow.notNil.if({
			now = Main.elapsedTime;
			newEvents = this.deepCopy.indexEnv(Env([prevIndex, index], [now - prevNow]), minDur).dataEvents;
			prevNow = now;
			prevIndex = index;
		}, {
			newEvents = this.deepCopy.indexEnv(Env([index, index], [0]), minDur).dataEvents;
			prevIndex = index;
			prevNow = Main.elapsedTime
		})
		^newEvents
	}

	firFilter { |whichKernel, dataType = 'val', eventType = 'events', method = 'fft'|
		var originalEvents;
		originalEvents = this.dataEvents.deepCopy;
		originalEvents.tracks.do{|track, trackNumber|
			var conv, clipSize, signal, mean, peak, size;
			clipSize = track.perform(eventType).size;
			signal = track.perform(eventType).collect{ |event| event.perform(dataType) }.copy.as(Signal);
			#signal, mean, peak, size = ftk.preConditionSignal(signal);
			conv = signal.convolve(whichKernel, method);
			conv = ftk.postConditionSignal(conv, mean, peak, size);
			this.dataEvents.tracks[trackNumber].perform(eventType).do{ |event, i| event.perform(dataType.asSetter, conv[i])};
		}
	}

	iirFilter { |dataType, eventType = 'events', filterType = 'LP', freq = 440, q = 1, sampleRate = 48000, removeBias = true, sos = true|
		var output, coeffs, signal, origSignal, freqArray, qArray, order;
		var mean, peak, size, originalEvents;
		originalEvents = this.dataEvents.deepCopy;
		originalEvents.tracks.do{|track, trackNumber|
			(track.notes.size > 0).if({
				// convert the freq and q into an array to be fed into the filter in case of an env
				freqArray = freq.isKindOf(Env).if({ freq[track.times] }, { freq.dup(track.perform(eventType).size) });
				qArray = q.isKindOf(Env).if({ q[track.times] }, { q.dup(track.perform(eventType).size) });
				// get an array of dataType values of the notes, convert to signal
				origSignal = track.perform(eventType).collect{ |note| note.perform(dataType) }.as(Signal);

				// remove bias (mostly for pitch)
				// translate signal from MIDI values (0 to 127) to (-1 to 1)
				// reverse signal and add to beginning of signal
				removeBias.if({
					#signal, mean, peak, size = ftk.preConditionSignal(origSignal.copy, true);
					// pad to next power of two, reverse then add to beginning like the data signal was
					freqArray = freqArray ++ freqArray.last.dup(size);
					freqArray = freqArray.reverse ++ freqArray;
					qArray = qArray ++ qArray.last.dup(size);
					qArray = qArray.reverse ++ qArray;
				}, {
					signal = origSignal.copy;
				});
				// run sos or fos filter
				sos.if({
					output = ftk.sos(signal, filterType, freqArray, qArray, sampleRate);
					output = ftk.sos(output.reverse, filterType, freqArray, qArray, sampleRate)
				}, {
					output = ftk.fos(signal, filterType, freqArray, sampleRate);
					output = ftk.fos(output.reverse, filterType, freqArray, sampleRate)
				});
				// put bias back
				removeBias.if({output = ftk.postConditionSignal(output.reverse, mean, peak, size, true)});
				this.dataEvents.tracks[trackNumber].perform(eventType).do{ |event, i| event.perform(dataType.asSetter, output.as(Array)[i]) }
			})
		}

	}

	sosRT { |event, dataType, dataRange = ([0, 1]), filterType = 'LP', freq = 440, q = 1, sampleRate = 48000|
		var res;
		res = ftk.sosFunc(event.perform(dataType).linlin(dataRange[0], dataRange[1], -1, 1), y1, y2, ftk.filtCoeffs.sosCoeffs(filterType, freq, q, sampleRate));
		y2 = res[1];
		y1 = res[2];
		^res[0].linlin(-1, 1, dataRange[0], dataRange[1])
	}

}